FROM docker.io/httpd:2.4

# Config
COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf

# Copy files
COPY ./index.html /usr/local/apache2/htdocs/
COPY ./logo_isric.svg /usr/local/apache2/htdocs/

# Permissions
RUN chgrp -R 0 /run && chmod -R g=u /run

EXPOSE 8080

USER 1001

