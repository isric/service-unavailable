#!/bin/sh

# Running it:
# docker build --force-rm -t $service .
# docker run -dit --name $service-local -p 8080:8080 $service

namespace="isric"
service="service-unavailable"
port="8080"

# Check arguments and set local variables
if [ -z "$1" ]
  then
    echo "Please provide a new tag as first argument"
    exit -1
fi

# Compile and tag image 
docker login docker-registry.wur.nl
docker build --no-cache -t docker-registry.wur.nl/$namespace/$service:$1 .

# Push image to GitLab registry
docker push docker-registry.wur.nl/$namespace/$service:$1

# Log on to Openshift and select project
oc login https://master.ocp.wurnet.nl
oc project special

# Delete previous deployment from OC
oc delete all --selector app=$service

# Wait a bit
echo "Waiting for OC to delete assets ..."
sleep 2

# Create new application in Openshiftt 
oc new-app docker-registry.wur.nl/$namespace/$service:$1
echo "Waiting for OC to create new app ..."
sleep 10
oc expose svc/$service --port=$port
# oc rollout latest dc/$service - not necessary

